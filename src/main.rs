use std::{
    convert::Infallible,
    io::{BufRead, BufReader},
    process::Stdio,
    time::SystemTime,
};

use duckdb::params;
use regex::Regex;

fn main() -> eyre::Result<Infallible> {
    let re =
        Regex::new(r"64 bytes from 1.1.1.1: icmp_seq=(\d+) ttl=53 time=([\d\.]+) ms\n").expect("valid");
    let now = SystemTime::now();
    std::fs::create_dir_all("/home/user/.local/share/ping-recorder/db")?;
    let db = duckdb::Connection::open(format!(
        "/home/user/.local/share/ping-recorder/db/pingdb-{}",
        now.duration_since(std::time::UNIX_EPOCH)
            .unwrap()
            .as_micros()
    ))?;
    db.execute(
        "create table if not exists pings (id UINTEGER, time FLOAT);",
        [],
    )?;

    let mut query_insert = db.prepare("insert into pings (id, time) values (?, ?);")?;

    let p = std::process::Command::new("ping")
        .arg("1.1.1.1")
        .stdout(Stdio::piped())
        .spawn()?;
    let p_stdout = p.stdout.expect("piped");
    // p_stdout.
    let mut buf_reader = BufReader::new(p_stdout);

    loop {
        let mut line = String::new();
        buf_reader.read_line(&mut line)?;
        if line.contains("bytes from") {
            // dbg!(&line);
            let groups = re.captures(&line).expect("Should match format");
            let a: u32 = groups[1].parse().expect("is int");
            let b: f32 = groups[2].parse().expect("is float");
            // dbg!(a, b);
            query_insert.execute(params![a, b])?;
        } else {
            // ignore unrelated lines
        }
    }
}
